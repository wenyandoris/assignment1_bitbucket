﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WenyanPatient_3.Models;

namespace WenyanPatient_3.Controllers
{
    // home controller is a default controller when you first come to the program
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        // home page: welcome page
        public IActionResult Index()
        {
            return View();
        }
        // shows site's privacy policy
        public IActionResult Privacy()
        {
            return View();
        }

        // leads to error page 
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
